package org.problem.solving;

import org.junit.jupiter.api.Test;
import org.problem.solving.common.ListNode;
import org.problem.solving.PalindromeLinkedList.PalindromeLinkedList;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class PalindromeLinkedListTest {

    @Test
    public void TestCase1() {
        ListNode node = new ListNode();
        ListNode head = node;

        node.val = 1;
        node.next = new ListNode();
        node = node.next;

        node.val = 2;
        node.next = new ListNode();
        node = node.next;

        node.val = 3;
        node.next = new ListNode();
        node = node.next;

        node.val = 2;
        node.next = new ListNode();
        node = node.next;

        node.val = 1;

        PalindromeLinkedList palindromeLinkedList = new PalindromeLinkedList();
        assertTrue(palindromeLinkedList.isPalindrome(head));
    }
}

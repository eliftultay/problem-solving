package org.problem.solving;

import org.junit.jupiter.api.Test;
import org.problem.solving.PalindromeLinkedList.PalindromeLinkedList;
import org.problem.solving.ReverseLinkedList.ReverseLinkedList;
import org.problem.solving.common.ListNode;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReverseLinkedListTest {


    @Test
    public void TestCase1() {
        ListNode node = new ListNode();
        ListNode head = node;

        node.val = 1;
        node.next = new ListNode();
        node = node.next;

        node.val = 2;
        node.next = new ListNode();
        node = node.next;

        node.val = 3;
        node.next = new ListNode();
        node = node.next;

        node.val = 4;
        node.next = new ListNode();
        node = node.next;

        node.val = 5;

        ReverseLinkedList reverseLinkedList = new ReverseLinkedList();
        ListNode reversedHead = reverseLinkedList.reverseList(head);
        assertEquals(5, reversedHead.val);
    }

}

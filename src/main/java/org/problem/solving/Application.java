package org.problem.solving;

import org.problem.solving.TrappingRainWater.TrappingRainWater;

public class Application {

    public static void main(String[] args) {

        trappingRainWater();

    }

    public static void trappingRainWater() {
        int [] height = new int[] {4,2,0,3,2,5};

        int sum = TrappingRainWater.trap(height);

        System.out.println(sum);
    }
}

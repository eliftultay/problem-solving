package org.problem.solving.ReverseLinkedList;

import org.problem.solving.common.ListNode;

public class ReverseLinkedList {

    public ListNode reverseList(ListNode head) {

        if(head == null) {
            return head;
        }

        if (head.next == null) {
            return head;
        }


        ListNode prev = head;
        ListNode current = head.next;
        ListNode next = head.next.next;

        prev.next = null;
        while (next != null) {
            current.next = prev;
            prev = current;
            current = next;
            next = next.next;
        }

        current.next = prev;

        return current;
    }
}

package org.problem.solving.PalindromeLinkedList;

import org.problem.solving.common.ListNode;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class PalindromeLinkedListUsingStackAndQueue {

    /*
    * Time Complexity = O(n)
    * Space Complexity = O(n)
    * */

    Stack<Integer> integerStack = new Stack<>();
    Queue<Integer> integerQueue = new LinkedList<>();

    public boolean isPalindrome(ListNode head) {

        if (head == null) {
            return false;
        }

        addListNodeToStackAndQueue(head);
        return popAndCompare(integerStack, integerQueue);
    }

    private void addListNodeToStackAndQueue(ListNode head) {
        while(head != null) {
            integerQueue.add(head.val);
            integerStack.push(head.val);
            head = head.next;
        }
    }

    private boolean popAndCompare(Stack<Integer> integerStack, Queue<Integer> integerQueue) {

        while (!integerStack.isEmpty()) {
            if (integerQueue.poll() != integerStack.pop()) {
                return false;
            }
        }
        return true;
    }
}

package org.problem.solving.PalindromeLinkedList;

import org.problem.solving.common.ListNode;

public class PalindromeLinkedList {

    /*
     * Time Complexity = O(n)
     * Space Complexity = O(n)
     * */

    public boolean isPalindrome(ListNode head) {

        if(head.next == null) {
            return true;
        }

        ListNode middle = head;
        ListNode doubleJumper = head;
        int counter = 1;

        while(doubleJumper.next != null && doubleJumper.next.next != null) {
            middle = middle.next;
            doubleJumper = doubleJumper.next.next;
            counter += 2;
        }

        if (doubleJumper.next != null) {
            counter++;
        }

        middle = middle.next;

        if(middle.next == null) {
            return head.val == middle.val;
        }

        ListNode prev = head;
        ListNode current = head.next;
        ListNode next = head.next.next;

        prev.next = null;
        while (next != middle) {
            current.next = prev;
            prev = current;
            current = next;
            next = next.next;
        }

        if (counter%2 ==0) {
            current.next = prev;
        } else {
            current = prev;
        }

        while(middle != null) {
            if(middle.val != current.val) {
                return false;
            }
            middle = middle.next;
            current = current.next;
        }

        return true;
    }
}

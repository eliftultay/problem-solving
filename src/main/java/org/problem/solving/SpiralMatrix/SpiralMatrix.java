package org.problem.solving.SpiralMatrix;

import java.util.ArrayList;
import java.util.List;

public class SpiralMatrix {

    public static void reachToMaximumIndex(int[][] matrix, List<Integer> list, int rowIndex, int columnIndex, int maxRow, int maxColumn, int layer) {
        while(columnIndex < maxColumn) {
            list.add(matrix[rowIndex][columnIndex]);
            columnIndex++;
        }

        columnIndex--;
        rowIndex++;

        while(rowIndex < maxRow) {
            list.add(matrix[rowIndex][columnIndex]);
            rowIndex++;
        }
        rowIndex--;
        columnIndex--;

        reachToMinimumIndex(matrix, list, rowIndex, columnIndex, layer);
    }

    public static void reachToMinimumIndex(int[][] matrix, List<Integer> list, int rowIndex, int columnIndex, int layer) {


        while(columnIndex >= layer) {
            list.add(matrix[rowIndex][columnIndex]);
            columnIndex--;
        }
        columnIndex++;
        rowIndex--;

        while(rowIndex > layer) {
            list.add(matrix[rowIndex][columnIndex]);
            rowIndex--;
        }
    }

    public static List<Integer> spiralOrder(int[][] matrix) {

        List<Integer> list = new ArrayList<>();
        int rowIndex = 0;
        int columnIndex = 0;
        int maxRow = matrix.length;
        int maxColumn = matrix[0].length;
        int layer = 0;
        int min = Math.min(maxRow, maxColumn);
        int maxLayer = min % 2 == 0 ? min - 1 : min;

        while(layer <= maxLayer) {
            reachToMaximumIndex(matrix, list, rowIndex, columnIndex, maxRow, maxColumn, layer);
            rowIndex++;
            columnIndex++;
            layer++;
            maxRow--;
            maxColumn--;
        }
        return list;
    }
}

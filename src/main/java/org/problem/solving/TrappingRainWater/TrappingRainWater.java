package org.problem.solving.TrappingRainWater;

public class TrappingRainWater {

    public static int trap(int[] height) {
        int sum = 0;

        return findTrappedRainWater(height, sum);
    }

    private static int findTrappedRainWater(int [] height, int sum) {

        int firstIndex = findFirstIndex(height);

        int lastIndex = findLastIndex(height);

        if (firstIndex == lastIndex) {
            return sum;
        }

        for (int i = firstIndex;  i <= lastIndex ; i++) {
            if (height[i] == 0) {
                sum ++;
            } else {
                height[i] = height[i] - 1;
            }
        }

        return findTrappedRainWater(height, sum);
    }

    private static int findFirstIndex(int [] height) {
        int firstIndex = -1;

        for (int i = 0; i < height.length; i++) {
            if (height[i] !=0) {
                firstIndex = i;
                break;
            }
        }
        return firstIndex;
    }

    private static int findLastIndex(int [] height) {
        int lastIndex = -1;

        for (int i = height.length-1; i >= 0; i--) {
            if (height[i] !=0) {
                lastIndex = i;
                break;
            }
        }
        return lastIndex;

    }
}
